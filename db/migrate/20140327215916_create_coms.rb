class CreateComs < ActiveRecord::Migration
  def change
    create_table :coms do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end
