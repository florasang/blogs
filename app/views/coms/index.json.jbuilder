json.array!(@coms) do |com|
  json.extract! com, :id, :post_id, :body
  json.url com_url(com, format: :json)
end
